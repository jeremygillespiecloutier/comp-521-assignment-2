﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Jeremy Gillespie-Cloutier (260688666)
 * This class handles the movement and the collision of projectiles
 */
public class Projectile : MonoBehaviour {

    public Vector3 velocity=Vector3.zero; // The projectile velocity
    public float PROJECTILE_SPEED=7; // The projectile speed
    private const float GRAVITY_FORCE = 12; // The projectile gravity force
    private Level level;
    private WindGenerator generator;
    private Collisions collisions;
    public float radius = 0.15f; // The radius of the projectile
    private float restitution = 0.9f; // The coefficient of restitution for collisions
    public float mass = 2; // The projectile mass

    void Start()
    {
        level = GameObject.Find("Level").GetComponent<Level>();
        generator = GameObject.Find("Level").GetComponent<WindGenerator>();
        collisions = GameObject.Find("Level").GetComponent<Collisions>();
	}

	void Update () {
        // If projectile is above bowl, apply wind as a force.
        if (transform.position.y > level.leftLip.p1.y)
            velocity += new Vector3(1, 0, 0) * generator.windSpeed/mass * Time.deltaTime;
        // Apply the force of gravity
        velocity = velocity + new Vector3(0, -GRAVITY_FORCE/mass * Time.deltaTime, 0);
        transform.position += velocity * Time.deltaTime; // update position

        // Check collisions with flies
        foreach (GameObject flyObj in GameObject.FindGameObjectsWithTag("fly"))
        {
            Fly fly = flyObj.GetComponent<Fly>();
            float diff = (transform.position - flyObj.transform.position).magnitude;
            // If the distance between fly and projectile is less than the sum of the fly and projectile radius, there is a collision
            if(diff<radius+fly.radius){
                // mark the fly as being hit and destroy the projectile
                fly.wasHit = true;
                Destroy(gameObject);
                break;
            }

        }

        // Check for collisions with the bowl and water
        CollisionResponse response = collisions.resolveCollision(level.waterPoints, true, transform.position, velocity, restitution, radius);
        if (!response.collided) response = collisions.resolveCollision(level.leftWall.points, false, transform.position, velocity, restitution, radius);
        if (!response.collided) response = collisions.resolveCollision(level.leftLip.points, false, transform.position, velocity, restitution, radius);
        if (!response.collided) response = collisions.resolveCollision(level.rightWall.points, false, transform.position, velocity, restitution, radius);
        if (!response.collided) response = collisions.resolveCollision(level.rightLip.points, false, transform.position, velocity, restitution, radius);
        if (response.collided) // If there is a collision
        {
            if (response.onWater) // If it was on water, destroy the projectile
                Destroy(gameObject);
            else // Otherwise seperate the projectile from the collision and apply new velocity
            {
                transform.position += response.seperation;
                velocity = response.newVelocity;
            }
        }
        // If the projectile is out of screen bounds (with an offset at top), destroy it.
        if (transform.position.x < Level.SCREEN_MIN.x || transform.position.x > Level.SCREEN_MAX.x ||
            transform.position.y < Level.SCREEN_MIN.y || transform.position.y > Level.SCREEN_MAX.y+4)
            Destroy(gameObject);
	}
}
