﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Jeremy Gillespie-Cloutier (260688666)
 * This class handles the generation of the bowl and the water
 */
public class Level : MonoBehaviour {

    public static Vector3 SCREEN_MIN, SCREEN_MAX; // The bottom left and top right corners in world coordinates
    private LineRenderer bowlRenderer; // Line renderer for the bowl
    private LineRenderer waterRenderer; // Line renderer for the water
    public BezierCurve leftWall, rightWall, floor, leftLip, rightLip; // Curves that form the bowl
    public List<Vector3> bowlPoints=new List<Vector3>(), waterPoints=new List<Vector3>(); // The points that form the bowl and the water
    private GameObject fish; // The fish object

    // Helper class that creates and discretizes a Bezier curves given 3 points
    public class BezierCurve
    {
        public Vector3 p0, p1, p2; // The 3 points defining the curve
        public List<Vector3> points; // The discretized points of the curve

        // p0, p1, p2 are 3 points defining the curve
        // numPoints is the number of points contained in the discretized version of the curve
        public BezierCurve(Vector3 p0, Vector3 p1, Vector3 p2, int numPoints)
        {
            this.p0 = p0;
            this.p1 = p1;
            this.p2 = p2;
            
            points = new List<Vector3>();
            float step = 1f / numPoints;
            bool lastAdded = false;
            for (float t = 0; t <= 1; t += step) // Seperate the curve in numPoints points
            {
                if (t == 1)
                    lastAdded = true; // Was the last point of the curve added
                Vector3 pos = getPosition(t);
                points.Add(pos);
            }
            if (!lastAdded) // Ensure last point of the curve gets added
                points.Add(getPosition(1));
        }

        // Calculates the position along the curve for parameter t in range [0,1]
        public Vector3 getPosition(float t)
        {
            t = Mathf.Clamp(t, 0, 1);
            return (1 - t) * ((1 - t) * p0 + t * p1) + t * ((1 - t) * p1 + t * p2);
        }
    }

	void Start () {
        SCREEN_MIN = Camera.main.ScreenToWorldPoint(Vector3.zero);
        SCREEN_MAX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        bowlRenderer = GameObject.Find("Bowl").GetComponent<LineRenderer>();
        waterRenderer = GameObject.Find("Water").GetComponent<LineRenderer>();
        fish = GameObject.Find("Fish");
        // Display the bowl and the generate the water with perlin noise
        displayBowl();
        generateWater();
	}

    // Generates the water with perlin noise
    void generateWater()
    {
        waterPoints.Clear();
        // The start and end points of the water along the left and right walls.
        // Water starts 70% from the top (or 30% from the floor) of the bowl
        Vector3 start = leftWall.getPosition(0.7f);
        Vector3 stop = rightWall.getPosition(0.3f);
        float width = stop.x - start.x; // The width of the water curve
        int numPoints=40; // Number of points in the water curve
        // Generate the water points at fixed intervals
        for(int i=1;i<numPoints;i++){
            Vector3 point=new Vector3(width/numPoints*i, start.y);
            waterPoints.Add(point);
        }
        // The 1 dimensional noise "grid"/vector
        List<float> noise = new List<float>();
        // Generate perlin noise at 7 different levels. At level i, the grid contains 2^i cells
        for (int i = 1; i <= 7; i++)
        {
            noise.Clear();
            int gridSize = (int)Mathf.Pow(2, i); // The number of cells in the noise vector
            // Choose a gradient from -1 to 1 for each cell
            for (int j = 0; j < gridSize; j++)
                noise.Add(Random.Range(-1f, 1f));
            float step = width / (gridSize-1);//The width between two cells of the perlin grid
            for (int j=0;j<waterPoints.Count;j++)
            {
                Vector3 point=waterPoints[j];
                // Determine in which portion of the perlin grid the water point lies.
                // Find the left and right points such that the water point's x value is between left.x and right.x
                // (The perlin cells are spaced at regular intervals)
                int leftCorner = (int)Mathf.Floor(point.x / step);
                int rightCorner = leftCorner + 1;
                float gradientLeft = noise[leftCorner]; // The gradient at the left cell
                float gradientRight = noise[rightCorner]; // The gradient at the right cell
                Vector3 pointX=new Vector3(point.x, 0, 0); // The water point, ignoring the y coordinate
                // Map the distance of the point to the left and right corners (such that distance from left cell to right cell=1)
                float distLeft=(pointX-new Vector3(leftCorner*step, 0, 0)).magnitude/step;
                float distRight=(new Vector3(rightCorner*step,0,0)-pointX).magnitude/step;
                // Interpolate between the left and right gradient according to the distance found
                float n = gradientLeft + interpolate(distLeft) * (gradientRight - gradientLeft);
                // inrement the y value of the water point by the (scaled) value of the noise calculated
                waterPoints[j] = new Vector3(point.x, point.y + n*0.2f, 0);
            }
        }
        // For simplicity the points' x values are between 0 and width, but we must offset them since the origin
        // on the screen is at the center
        Vector3 offset = new Vector3(width / 2, 0, 0);
        // Add start and end points (including offset since we will substract it in the loop after)
        waterPoints.Insert(0, start+offset);
        waterPoints.Add(stop+offset);
        waterRenderer.positionCount = waterPoints.Count;
        // Substract the offset to get the actual point poisitions on the screen, and add them to the water line renderer
        for (int i = 0; i < waterPoints.Count; i++)
        {
            waterPoints[i] -= offset;
            waterRenderer.SetPosition(i, waterPoints[i]);
        }
        waterRenderer.startWidth = 0.1f;
        waterRenderer.endWidth = 0.1f;
        // Position the fish at the center of the water line
        fish.transform.position = new Vector3(0, start.y, 0);
    }

    // The interpolation function for perlin noise, I used 3x^2-2x^3
    float interpolate(float x)
    {
        return 3 * Mathf.Pow(x, 2) - 2 * Mathf.Pow(x, 3);
    }

    // Displays the bowl
    void displayBowl()
    {
        bowlPoints.Clear();
        // These are the points that will be used in the 5 bezier curves
        // There is one for the left and right lips and walls of the bowl, and one for the floor
        Vector3 l1 = new Vector3(-9, 2, 0);
        Vector3 l2 = new Vector3(-8.5f, 2.5f, 0);
        Vector3 p0 = new Vector3(-8, 2, 0);
        Vector3 p1 = new Vector3(-9, -0.5f, 0);
        Vector3 p2 = new Vector3(-8, -3, 0);
        Vector3 p3 = new Vector3(0, -6, 0);
        Vector3 p4 = new Vector3(8, -3, 0);
        Vector3 p5 = new Vector3(9, -0.5f, 0);
        Vector3 p6 = new Vector3(8, 2, 0);
        Vector3 l3 = new Vector3(8.5f, 2.5f, 0);
        Vector3 l4 = new Vector3(9, 2, 0);
        leftLip = new BezierCurve(l1, l2, p0, 10);
        leftWall = new BezierCurve(p0, p1, p2, 10);
        floor = new BezierCurve(p2, p3, p4, 10);
        rightWall = new BezierCurve(p4, p5, p6, 10);
        rightLip = new BezierCurve(p6, l3, l4, 10);
        // Add the points from each curve to the bowl points
        bowlPoints.AddRange(leftLip.points);
        bowlPoints.AddRange(leftWall.points);
        bowlPoints.AddRange(floor.points);
        bowlPoints.AddRange(rightWall.points);
        bowlPoints.AddRange(rightLip.points);
        // Render these points with the bowl line renderer
        bowlRenderer.positionCount = bowlPoints.Count;
        for (int i = 0; i < bowlPoints.Count; i++)
            bowlRenderer.SetPosition(i, bowlPoints[i]);
        bowlRenderer.startWidth = 0.1f;
        bowlRenderer.endWidth = 0.1f;
    }

}
