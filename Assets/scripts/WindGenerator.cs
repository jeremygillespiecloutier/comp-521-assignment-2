﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Jeremy Gillespie-Cloutier (260688666)
 * This class generates the wind and spawns the flies
 */
public class WindGenerator : MonoBehaviour {

    float windTimeElapsed = 0, flyTimeElapsed=0; // Time elapsed since the last wind speed change and fly spawn
    float totalWindTime, totalFlyTime; // The total time before the wind changes or a new fly spawns (not counting time elapsed so far)
    public float windSpeed; // The wind speed
    float screenWidth; //The width of the game screen in game units
    Level level;
    private const float MIN_FLY = 3f, MAX_FLY = 6f, MIN_WIND = 1f, MAX_WIND = 2f; // The min and max values the total wind and spawn time can take

	void Start () {
        level = GameObject.Find("Level").GetComponent<Level>();
        // Choose a wind duration randomly (between 1 and 2 seconds)
        totalWindTime = Random.Range(MIN_WIND, MAX_WIND);
        // Choose the time for next fly to spawn (3 to 6 seconds)
        totalFlyTime = Random.Range(MIN_FLY, MAX_FLY);
        screenWidth = Level.SCREEN_MAX.x - Level.SCREEN_MIN.x; // Calculate screen width
        // Randomly choose the wind speed such that it takes from 2 to 10 seconds to traverse
        windSpeed = Random.Range(screenWidth / 10f, screenWidth / 2f);
	}

	void Update () {
        // Update the wind and fly counters
        windTimeElapsed += Time.deltaTime;
        flyTimeElapsed += Time.deltaTime;
        if (windTimeElapsed > totalWindTime) // wind counter reached total wind time
        {
            // Reset wind counter (not to 0, keep the exceeding time)
            windTimeElapsed -= totalWindTime;
            // Randomly choose another total wind time and speed 
            totalWindTime = Random.Range(MIN_WIND, MAX_WIND);
            windSpeed = Random.Range(screenWidth / 10f, screenWidth / 2f);
        }
        if (flyTimeElapsed > totalFlyTime) // fly sapwn time counter reached total fly spawn time
        {
            flyTimeElapsed -= totalFlyTime; // Reset counter (not to 0, keep exceeding time)
            totalFlyTime = Random.Range(MIN_FLY, MAX_FLY); // Randomly choose next spawn time
            // Instantiate a fly
            GameObject fly = (GameObject)Instantiate(Resources.Load("Fly"));
            float flySize = fly.GetComponent<SpriteRenderer>().bounds.extents.y;
            // Find the y range above the bowl in whcih the fly can spawn, randomly choose a value within, and position the fly there
            float minY = level.leftLip.p1.y + flySize;
            float maxY = Level.SCREEN_MAX.y - flySize;
            float flySpawn = Random.Range(minY, maxY);
            fly.transform.position = new Vector3(Level.SCREEN_MIN.x, flySpawn, 0);
        }
	}
}
