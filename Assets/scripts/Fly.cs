﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Jeremy Gillespie-Cloutier (260688666)
 * This class handles the movement and the collisions of flies
 */
public class Fly : MonoBehaviour {

    private WindGenerator generator;
    public bool wasHit = false; // Flag indicating if the fly was hit by a projectile
    public float radius = 0.75f; // Radius of the circle collider
    private float restitution = 0.5f; // Coefficient of restitution
    private Collisions collisions; 
    private Level level;
    private Vector3 velocity; // The fly velocity
    private const float ACCELERATION = 8f; // The fly gravity acceleration

	void Start () {
        level = GameObject.Find("Level").GetComponent<Level>();
        generator = GameObject.Find("Level").GetComponent<WindGenerator>();
        collisions = GameObject.Find("Level").GetComponent<Collisions>();
	}

	void Update () {
        // If the fly was not hit, the velocity matches the wind speed
        if (!wasHit)
            velocity= new Vector3(1,0,0)*generator.windSpeed;
        else // Otherwise, velocity accelerates downards according to gravity acceleration
            velocity -= new Vector3(0, ACCELERATION * Time.deltaTime, 0);

        // Check for collisions between the bowl and the water
        CollisionResponse response = collisions.resolveCollision(level.waterPoints, true, transform.position, velocity, restitution, radius);
        if (!response.collided) response = collisions.resolveCollision(level.leftLip.points, false, transform.position, velocity, restitution, radius);
        if (!response.collided) response = collisions.resolveCollision(level.leftWall.points, false, transform.position, velocity, restitution, radius);
        if (!response.collided) response = collisions.resolveCollision(level.rightLip.points, false, transform.position, velocity, restitution, radius);
        if (!response.collided) response = collisions.resolveCollision(level.rightWall.points, false, transform.position, velocity, restitution, radius);
        // If there is a collision
        if (response.collided){
            if (response.onWater) // When it is on water, destroy the fly
                Destroy(gameObject);
            else // Otherwise update seperate the fly from the object it collided into and apply new velocity
            {
                transform.position += response.seperation;
                velocity = response.newVelocity;
            }
        }
        transform.position += velocity * Time.deltaTime; // Update position
        // If fly is out of screen bounds delete it
        if (transform.position.x < Level.SCREEN_MIN.x-5 || transform.position.x > Level.SCREEN_MAX.x ||
            transform.position.y < Level.SCREEN_MIN.y || transform.position.y > Level.SCREEN_MAX.y)
            Destroy(gameObject);
	}
}
