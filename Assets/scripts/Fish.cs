﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Jeremy Gillespie-Cloutier (260688666)
 * This class handles the movement, collision detection and projectile spawning of the fish
 */
public class Fish : MonoBehaviour {

    private float rotation = 0; // The fish rotation from the vertical
    private float position = 0; // The fish x position
    private const float ROTATION_SPEED=120; // degrees per second
    private const float MOVE_SPEED = 4; // World units per second
    private Quaternion initialRotation; // The rotation quaternion of the fish when rotation=0
    private Level level;
    private Vector3 corner1, corner2, corner3, corner4;// The 4 corners of the fish bounding box
    // Width from center to edge and height from center to top of bounding box
    // technically they should be called halfWidth and halfHeight but I wanted to make this simple.
    private float width = 0.6f, height = 0.9f;

	void Start () {
        initialRotation = transform.rotation;
        level=GameObject.Find("Level").GetComponent<Level>();
	}

    void Update()
    {
        // Update the rotation if W or S is pressed and the position if A or D is pressed
        if (Input.GetKey(KeyCode.W))
            rotation -= ROTATION_SPEED * Time.deltaTime;
        if (Input.GetKey(KeyCode.S))
            rotation += ROTATION_SPEED * Time.deltaTime;
        if (Input.GetKey(KeyCode.A))
            position -= MOVE_SPEED * Time.deltaTime;
        if (Input.GetKey(KeyCode.D))
            position += MOVE_SPEED * Time.deltaTime;

        rotation = Mathf.Clamp(rotation, -60, 60); //Ensure rotation is within -60 to 60
        Quaternion newRotation = Quaternion.Euler(0, 0, rotation); // The rotation quaternion
        transform.rotation = newRotation * initialRotation; // Apply the rotation to fish
        transform.position = new Vector3(position, transform.position.y, 0); // Position the fish
        // Calculate the position of the fishe's corners given the fish rotation and position
        corner1 = newRotation * new Vector3(-width, -height, 0) + transform.position;
        corner2 = newRotation * new Vector3(-width, height, 0) + transform.position;
        corner3 = newRotation * new Vector3(width, height, 0) + transform.position;
        corner4 = newRotation * new Vector3(width, -height, 0) + transform.position;
        
        checkCollisions(); // Check for collisions
        // If the space key was pressed, create projectiles.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < 3; i++) // There are 3 projectiles
            {
                float scale = Random.Range(1, 2f); // Random projectile size
                GameObject projectileObj = (GameObject)Instantiate(Resources.Load("Projectile"));
                Projectile projectile = projectileObj.GetComponent<Projectile>();
                Vector3 direction = newRotation * new Vector3(0, 1, 0); // The direction in which the projectile moves
                Vector3 right = newRotation * new Vector3(1, 0, 0); // The left/right axis from where the fish looks
                // Bullet's position offset from where the fish mouth is, differrent values for the 3 projectile
                Vector3 offset = direction * 0.85f;
                if (i == 1)
                    offset = direction * 0.4f + right * 0.5f;
                else if (i == 2)
                    offset = direction * 0.4f - right * 0.5f;
                // Position and scale the projectile, set it's velocity, and also scale it's radius and mass
                projectileObj.transform.position = transform.position + direction * height + offset;
                projectileObj.transform.localScale *= scale;
                projectile.velocity = direction * projectile.PROJECTILE_SPEED;
                projectile.radius *= scale;
                projectile.mass *= scale;
            }
        }
        
    }

    // Checks for collisions with the walls of the bowl
    void checkCollisions()
    {
        // Check collisions for each segment of the left and right walls
        for (int i = 1; i < level.leftWall.points.Count; i++)
        {
            Vector3 p1 = level.leftWall.points[i];
            Vector3 p2 = level.leftWall.points[i - 1];
            checkSegment(p1, p2, -1); // Call with sign=-1, as we want to know if a corner is to the left
        }
        for (int i = 1; i < level.rightWall.points.Count; i++)
        {
            Vector3 p1 = level.rightWall.points[i-1];
            Vector3 p2 = level.rightWall.points[i];
            checkSegment(p1, p2, 1); // Call with sign=1, we want to know if corner is to the right
        }
    }

    // Checks, for a specific segment, if either of the fishes corner lies on the other side of the segment
    // The parameter sign should be 1 if we check if the fish is to the right of the segment, and -1 if to the left.
    void checkSegment(Vector3 p1, Vector3 p2, int sign)
    {
        // Calculate the slope y=slope*x+b
        float slope = (p2.y - p1.y) / (p2.x - p1.x);
        float b = p1.y - slope * p1.x;
        float seperation = 0;
        // If the corner's y value is within the segment's y range, calculate x seperation (overlap)
        if (corner1.y >= p1.y && corner1.y <= p2.y)
            seperation = Mathf.Max(distancePointLine(slope, b, corner1)*sign, seperation);
        if (corner2.y >= p1.y && corner2.y <= p2.y)
            seperation = Mathf.Max(distancePointLine(slope, b, corner2)*sign, seperation);
        if (corner3.y >= p1.y && corner3.y <= p2.y)
            seperation = Mathf.Max(distancePointLine(slope, b, corner3)*sign, seperation);
        if (corner4.y >= p1.y && corner4.y <= p2.y)
            seperation = Mathf.Max(distancePointLine(slope, b, corner4)*sign, seperation);
        if (seperation > 0)
        {
            // If there is some overlap outside the bowl, move fish in opposite direction by the overlap
            position -= seperation*sign;
            transform.position = new Vector3(position, transform.position.y, 0);
        }
    }

    // Returns the distance between a point and a line y=slope*x+b
    float distancePointLine(float slope, float b, Vector3 point)
    {
        float x2 = (point.y - b) / slope;
        return point.x - x2;
    }
}
