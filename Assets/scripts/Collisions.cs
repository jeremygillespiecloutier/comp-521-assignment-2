﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Jeremy Gillespie-Cloutier (260688666)
 * General collision class to detect collisions between some shapes and gather collision info (normal, collision depth, ...)
 */
public class Collisions : MonoBehaviour {

    // Returns information about collisions between a circle of given radius and center and a segment (p1,p2)
    public CollisionInfo collisionCircleSegment(Vector3 center, float radius, Vector3 p1, Vector3 p2)
    {
        // Project the vector center-p1 on the vector v1-v2
        Vector3 v1 = p2 - p1;
        Vector3 v2 = center - p1;
        Vector3 proj = Vector3.Dot(v2, v1) / Mathf.Pow(v1.magnitude, 2) * v1;
        // This allows us to find the closest point on the segment from the circle center
        Vector3 closest = p1 + proj;
        Vector3 normal = center - closest; // The normal is the vector from closest point to center
        // If the normal 9before it is normalized) has magnitude smaller than circle radius, and closest point is between p1-p2, there is a collision
        if (normal.magnitude < radius && inBetween(closest, p1, p2))
        {
            // Return information about the collision
            CollisionInfo info = new CollisionInfo();
            info.point = closest;
            info.penetration = radius - normal.magnitude;
            normal.Normalize();
            info.normal = Vector3.Normalize(normal);
            return info;
        }
        return null; // If no collision occured, return null
    }

    // Returns true if the point v lies on the segment ab
    public bool inBetween(Vector3 v, Vector3 a, Vector3 b)
    {
        return Mathf.Abs((a - v).magnitude + (b - v).magnitude - (b - a).magnitude) <= 0.01f; // Add epsilon for floating point innacuracy
    }

    // Resolves a collision between a circle and a curve (bowl or water).
    // Points is the list of points in the curve, water is true if the curve is the water curve, position is the cirlc eposition,
    // velocity is the velocity of the object that has the circle collider, resitution is the coefficient of restitution (between 0 and 1)
    // and radius is the circle's radius
    public CollisionResponse resolveCollision(List<Vector3> points, bool water, Vector3 position, Vector3 velocity, float restitution, float radius)
    {
        CollisionResponse response = new CollisionResponse();
        for (int i = 1; i < points.Count; i++) // Check each segment of the curve
        {
            Vector3 p1 = points[i - 1];
            Vector3 p2 = points[i];
            // Check if there is a collision between circle and segment p1-p2
            CollisionInfo info = collisionCircleSegment(position, radius, p1, p2);
            if (info != null)
            {
                // If ther is, return information about the physics response of the collision
                response.info = info;
                response.onWater=water;
                response.seperation = info.normal * info.penetration;
                // Calculate the angle between initial velocity and the normal
                float angle = Vector3.SignedAngle(info.normal, -velocity, new Vector3(0, 0, 1));
                // The new velocity is at the same angle but on the other side of the normal, and it is scaled by the restitution coeff.
                response.newVelocity = Quaternion.Euler(0, 0, -angle) * info.normal * velocity.magnitude * restitution;
                response.collided=true;
                return response;
            }
        }
        return response; // If no collision, return a response with collided set to false
    }
}

// Contains information about the response to a collision
public class CollisionResponse
{
    public bool collided=false, onWater;
    public Vector3 newVelocity; // new velocity after collision
    public Vector3 seperation; // How much the objects need to be seperated
    public CollisionInfo info; // collision info
}

// Contains information about a collision
public class CollisionInfo
{
    public Vector3 point; // The collision point
    public Vector3 normal; // The collision normal
    public float penetration; //The amount of penetration in the collision
}
